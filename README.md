# A simple javascript parser for google sheets#

It consumes sheet data via api urls in this form:
https://spreadsheets.google.com/feeds/cells/DOCUMENT_KEY/WORKSHEET/public/full?alt=json&callback=CALLBACK_FUNCTION

The working example, index.html, is just a rough start. it uses d3js (from a remote cdn) 
to render the data fetched and parsed from the google endpoint as a table.